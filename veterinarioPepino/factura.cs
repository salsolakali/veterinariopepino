﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace veterinarioPepino
{
    public partial class Factura : Form
    {
        Document document = new Document();
        String nombreFactura;
        public static DataTable compras = new DataTable();
        private static Factura _instancia;

        public static Factura Instancia
        {
            get
            {
                if (_instancia == null)

                    _instancia = new Factura();
                return _instancia;


            }
        }
        public Factura()
        {
            InitializeComponent();
            dataGridView1.DataSource = TiendaClientes.compras;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ExportToPdf(TiendaClientes.compras);
            MessageBox.Show("Se ha enviado la factura a su correo");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ExportToPdf(TiendaClientes.compras);
            EnviaCorreo();
            MessageBox.Show("Se ha generado la factura correctamente");
        }
        public void EnviaCorreo()
        {
            
            MailMessage Mensaje = new MailMessage();

            Mensaje.To.Add(new MailAddress(Microsoft.VisualBasic.Interaction.InputBox("Introduce el correo", "Title", "Ejemplo@ejemplo.com")));
            Mensaje.From = new MailAddress("dano.garcia.jimenez@gmail.com");
            Mensaje.Subject = "Correo enviado desde visual studio";
            Mensaje.Body = "Saludos desde DAM!";

            System.Net.Mime.ContentType contentType = new System.Net.Mime.ContentType();
            contentType.MediaType = System.Net.Mime.MediaTypeNames.Application.Octet;
            contentType.Name = nombreFactura + ".pdf";
            Mensaje.Attachments.Add(new Attachment(@"C:\Users\Dani\Documents\Facturas\" + nombreFactura + ".pdf", contentType));

            SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
            client.EnableSsl = true;

            NetworkCredential credentials = new NetworkCredential("dano.garcia.jimenez@gmail.com", "bobesponja1", "");
            client.Credentials = credentials;

            try
            {
                client.Send(Mensaje);
                Console.WriteLine("Envio realizado.");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error:" + ex.ToString());
            }

        }
            public void ExportToPdf(DataTable dt)
        {
            DateTime time = DateTime.Now;
            nombreFactura =time.ToString("yyyyMMMdd-Hmmss");

            //DANI
            PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(@"C:\Users\Dani\Documents\Facturas\" + nombreFactura + ".pdf", FileMode.Create));

            //PAPI
            //PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(@"C:\Users\USUARIO\Documents\Facturas\" +nombreFactura +".pdf", FileMode.Create));
            document.Open();
            iTextSharp.text.Font font5 = iTextSharp.text.FontFactory.GetFont(FontFactory.HELVETICA, 5);

            PdfPTable table = new PdfPTable(dt.Columns.Count);
            PdfPRow row = null;
            //SALTA ERROR AQUI
            float[] widths = new float[] { 4f, 4f, 4f, 4f,4f};

            table.SetWidths(widths);

            table.WidthPercentage = 100;
            int iCol = 0;
            string colname = "";
            PdfPCell cell = new PdfPCell(new Phrase("Products"));

            cell.Colspan = dt.Columns.Count;

            foreach (DataColumn c in dt.Columns)
            {

                table.AddCell(new Phrase(c.ColumnName, font5));
            }

            foreach (DataRow r in dt.Rows)
            {
                if (dt.Rows.Count > 0)
                {
                    table.AddCell(new Phrase(r[0].ToString(), font5));
                    table.AddCell(new Phrase(r[1].ToString(), font5));
                    table.AddCell(new Phrase(r[2].ToString(), font5));
                    table.AddCell(new Phrase(r[3].ToString(), font5));
                    table.AddCell(new Phrase(r[4].ToString(), font5));
                }
            }
            document.Add(table);
            document.Close();
        }
    }
}
