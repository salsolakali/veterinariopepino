﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace veterinarioPepino
{
    public partial class Mascotas : UserControl
    {

        public int numeroFilas;
        public int numeroFilasVisitas;
        Boolean estoyEnDetalles = false;
        String consulta = "";
        String idAnimalAmostrarDetalles;
        
        private static Mascotas _instancia;
        public static Mascotas Instancia
        {
            get
            {
                if (_instancia == null)

                    _instancia = new Mascotas();

                return _instancia;


            }
        }

        public Mascotas()
        {
            InitializeComponent();
            rellenaList();
            dataGridViewVisitas.Visible = false;
            numeroFilas = dataGridViewMascotas.Rows.Count - 1;
            numeroFilasVisitas = dataGridViewVisitas.Rows.Count - 1;
            btnVolver.Hide();
        }

        public void rellenaList()
        {
            string query = "SELECT * FROM MASCOTAS";
            Conexion verMascotas = new Conexion(query);

            DataTable datos = verMascotas.resultadoConsulta(query);
            dataGridViewMascotas.DataSource = datos;

        }


        private void button5_Click(object sender, EventArgs e)
        {
            //Boton de actualizar
            rellenaList();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (!estoyEnDetalles)
            {
                //Boton de guardar
                String nombre = dataGridViewMascotas.CurrentRow.Cells["nombre"].Value.ToString();
                String especie = dataGridViewMascotas.CurrentRow.Cells["especie"].Value.ToString();
                String raza = dataGridViewMascotas.CurrentRow.Cells["raza"].Value.ToString();
                String chip = dataGridViewMascotas.CurrentRow.Cells["chip"].Value.ToString();
                String fechaNacimiento = transformaLaFechaEnFormatoMySql(dataGridViewMascotas.CurrentRow.Cells["fechaNAcimiento"].Value.ToString());
                String propietario = dataGridViewMascotas.CurrentRow.Cells["propietario"].Value.ToString();
                String idMascota = dataGridViewMascotas.CurrentRow.Cells["idMascota"].Value.ToString();

                if (dataGridViewMascotas.CurrentRow.Index >= numeroFilas)
                {
                    String query = "INSERT INTO `mascotas`(`nombre`, `especie`, `raza`, `chip`, `fechaNacimiento`, `propietario`) VALUES('" + nombre + "','" + especie + "','" + raza + "','" + chip + "','" + fechaNacimiento + "','" + propietario + "')";

                    Conexion insercionMascota = new Conexion(query);

                    MessageBox.Show("Insercion realizada correctamente");
                    numeroFilas = dataGridViewMascotas.Rows.Count - 1;
                }
                else
                {
                    String query = "UPDATE `mascotas` SET `nombre`='" + nombre + "',`especie`='" + especie + "',`raza`='" + raza + "',`chip`='" + chip + "',`fechaNacimiento`='" + fechaNacimiento + "',`propietario`='" + propietario + "' WHERE `idMascota` = '" + idMascota + "'";
                   
                    Conexion insercionMascota = new Conexion(query);

                    MessageBox.Show("Insercion realizada correctamente");
                    numeroFilas = dataGridViewMascotas.Rows.Count - 1;
                }
            }
            else {
                
                String fecha = transformaLaFechaEnFormatoMySql(dataGridViewVisitas.CurrentRow.Cells["fecha"].Value.ToString());
                String consulta = dataGridViewVisitas.CurrentRow.Cells["consulta"].Value.ToString();
                String atendidoPor = dataGridViewVisitas.CurrentRow.Cells["atendidoPor"].Value.ToString();

                
                if (dataGridViewVisitas.CurrentRow.Index >= numeroFilasVisitas)
                {
                    String query = "INSERT INTO `visitas`(`mascota`, `fecha`, `consulta`, `atendidoPor`) VALUES('" + idAnimalAmostrarDetalles + "','" + fecha + "','" + consulta + "','" + atendidoPor + "')";
                    
                    Conexion insercionMascota = new Conexion(query);

                    MessageBox.Show("Insercion realizada correctamente");
                    numeroFilas = dataGridViewVisitas.Rows.Count - 1;
                }
            }
        }

        private String transformaLaFechaEnFormatoMySql(String fechaEntrada)
        {
            //Este metodo transforma la fecha de entrada en un formato correcto para la query de Mysql
            String fechaQueEntra = fechaEntrada;

            return fechaQueEntra.Substring(6, 4) + "-" + fechaQueEntra.Substring(3, 2) + "-" + fechaQueEntra.Substring(0, 2);

        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (!estoyEnDetalles)
            {
                //Boton de borrar mascota
                String idABorrar = dataGridViewMascotas.CurrentRow.Cells["idMascota"].Value.ToString();

                String query = "DELETE FROM `MASCOTAS` WHERE `mascotas`.`idMascota` = " + idABorrar;
                Conexion borradoMascota = new Conexion(query);
                rellenaList();
            }
            else {
                
                String query = "DELETE FROM `visitas` WHERE `visitas`.`mascota` = " + idAnimalAmostrarDetalles;
                Conexion borradoMascota = new Conexion(query);
                rellenaList();
            }
            
        }
    

        private void textBox2_KeyUp(object sender, KeyEventArgs e)
        {
            
            if (e.KeyCode == Keys.Delete)
            {
                consulta = consulta.Substring(0, -1);
                string query = "SELECT * FROM MASCOTAS WHERE nombre LIKE '%" + consulta + "%' OR especie  LIKE '%" + consulta + "%' OR raza  LIKE '%" + consulta + "%' OR chip  LIKE '%" + consulta + "%' OR fechaNacimiento  LIKE '%" + consulta + "%' OR propietario  LIKE '%" + consulta + "%'";
                Conexion verMascotas = new Conexion(query);

                DataTable datos = verMascotas.resultadoConsulta(query);
                dataGridViewMascotas.DataSource = datos;
            }
            else
            {
                consulta = ((TextBox)sender).Text;
                string query = "SELECT * FROM MASCOTAS WHERE nombre LIKE '%" + consulta + "%' OR especie  LIKE '%" + consulta + "%' OR raza  LIKE '%" + consulta + "%' OR chip  LIKE '%" + consulta + "%' OR fechaNacimiento  LIKE '%" + consulta + "%' OR propietario  LIKE '%" + consulta + "%'";
                
                Conexion verMascotas = new Conexion(query);

                DataTable datos = verMascotas.resultadoConsulta(query);
                dataGridViewMascotas.DataSource = datos;
            }
            if (textBox2.Text == "")
            {
                consulta = "";
                string query = "SELECT * FROM MASCOTAS";
                Conexion verMascotas = new Conexion(query);

                DataTable datos = verMascotas.resultadoConsulta(query);
                dataGridViewMascotas.DataSource = datos;
            }
        }

        private void dataGridView2_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            //Cuando hago doble click en la celda de un bicho, se abre un datagridview con la informacion de las visitas de ese animal

            estoyEnDetalles = true;
            dataGridViewVisitas.Visible = true;
           
            idAnimalAmostrarDetalles = dataGridViewMascotas.CurrentRow.Cells["idMascota"].Value.ToString();

            
            btnVolver.Show();
            string query = "SELECT `fecha`, `consulta`, `atendidoPor` FROM `visitas` WHERE `idVisita`= " + idAnimalAmostrarDetalles;
            Conexion verDetalles = new Conexion(query);

            DataTable datos = verDetalles.resultadoConsulta(query);
            dataGridViewVisitas.DataSource = datos;

        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            btnVolver.Hide();
            estoyEnDetalles = false;
            dataGridViewVisitas.Visible = false;
            rellenaList();
        }
    }
}
