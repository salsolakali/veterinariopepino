﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace veterinarioPepino
{
    public partial class form : Form
    {
        public Point posicionRaton;
        
        public form()
        {
            InitializeComponent();
            Bunifu.Framework.Lib.Elipse.Apply(this, 5);
            label2.Text = Login.nombreUsuario;
    }

        private void Inicio_MouseDown(object sender, MouseEventArgs e)
        {
            posicionRaton = new Point(-e.X, -e.Y);
        }

        private void Inicio_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                Point posRaton = Control.MousePosition;
                posRaton.Offset(posicionRaton.X, posicionRaton.Y);
                Location = posRaton;
            }
        }

        private void bunifuFlatButton1_Click(object sender, EventArgs e)
        {
            if (!panelInicio.Controls.Contains(Clientes.Instancia))
            {
                panelInicio.Controls.Add(Clientes.Instancia);
                Clientes.Instancia.Dock = DockStyle.Fill;
                Clientes.Instancia.BringToFront();
            }
            else {
                Clientes.Instancia.BringToFront();
            }
        }

        private void cabecera_MouseDown(object sender, MouseEventArgs e)
        {
            posicionRaton = new Point(-e.X, -e.Y);
        }

        private void cabecera_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                Point posRaton = Control.MousePosition;
                posRaton.Offset(posicionRaton.X, posicionRaton.Y);
                Location = posRaton;
            }
        }

        private void bunifuFlatButton2_Click(object sender, EventArgs e)
        {
            if (!panelInicio.Controls.Contains(Mascotas.Instancia))
            {
                panelInicio.Controls.Add(Mascotas.Instancia);
                Mascotas.Instancia.Dock = DockStyle.Fill;
                Mascotas.Instancia.BringToFront();
            }
            else
            {
                Mascotas.Instancia.BringToFront();
            }
        }

        private void bunifuFlatButton3_Click(object sender, EventArgs e)
        {
            if (!panelInicio.Controls.Contains(Usuarios.Instancia))
            {
                panelInicio.Controls.Add(Usuarios.Instancia);
                Usuarios.Instancia.Dock = DockStyle.Fill;
                Usuarios.Instancia.BringToFront();
            }
            else
            {
                Usuarios.Instancia.BringToFront();
            }
        }

        private void bunifuFlatButton4_Click(object sender, EventArgs e)
        {
            if (!panelInicio.Controls.Contains(TiendaClientes.Instancia))
            {
                panelInicio.Controls.Add(TiendaClientes.Instancia);
                TiendaClientes.Instancia.Dock = DockStyle.Fill;
                TiendaClientes.Instancia.BringToFront();
            }
            else
            {
                TiendaClientes.Instancia.BringToFront();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void bunifuFlatButton5_Click(object sender, EventArgs e)
        {
            if (!panelInicio.Controls.Contains(TiendaAdmin.Instancia))
            {
                panelInicio.Controls.Add(TiendaAdmin.Instancia);
                TiendaAdmin.Instancia.Dock = DockStyle.Fill;
                TiendaAdmin.Instancia.BringToFront();
            }
            else
            {
                TiendaAdmin.Instancia.BringToFront();
            }
        }

        private void panelLateral_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Factura_Click(object sender, EventArgs e)
        {
            if (!panelInicio.Controls.Contains(veterinarioPepino.Factura.Instancia))
            {
                panelInicio.Controls.Add(veterinarioPepino.Factura.Instancia);
                veterinarioPepino.Factura.Instancia.Dock = DockStyle.Fill;
                veterinarioPepino.Factura.Instancia.BringToFront();
            }
            else
            {
                veterinarioPepino.Factura.Instancia.BringToFront();
            }
        }
    }
}
