﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace veterinarioPepino
{
    public partial class Usuarios : UserControl
    {
        public int numeroFilas;
        String consulta = "";
        private static Usuarios _instancia;
        public static Usuarios Instancia
        {
            get
            {
                if (_instancia == null)

                    _instancia = new Usuarios();
                return _instancia;


            }
        }
        public Usuarios()
        {
            InitializeComponent();
            rellenaList();
            numeroFilas = dataGridViewUsuarios.Rows.Count - 1;
        }
        public void rellenaList()
        {
            string query = "SELECT * FROM USUARIOS";
            Conexion verUsuarios = new Conexion(query);

            DataTable datos = verUsuarios.resultadoConsulta(query);
            dataGridViewUsuarios.DataSource = datos;

        }

        private void button5_Click(object sender, EventArgs e)
        {
            //Actualizar
            rellenaList();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            //Guardar usuario
            String nombre = dataGridViewUsuarios.CurrentRow.Cells["nombre"].Value.ToString();
            String apellidos = dataGridViewUsuarios.CurrentRow.Cells["apellidos"].Value.ToString();
            String dni = dataGridViewUsuarios.CurrentRow.Cells["dni"].Value.ToString();
            String contrasena = dataGridViewUsuarios.CurrentRow.Cells["contrasena"].Value.ToString();
            String id = dataGridViewUsuarios.CurrentRow.Cells["id"].Value.ToString();

            if (dataGridViewUsuarios.CurrentRow.Index >= numeroFilas)
            {
                String query = "INSERT INTO `USUARIOS`(`nombre`, `apellidos`, `dni`, `contrasena`) VALUES('" + nombre + "','" + apellidos + "','" + dni + "','" + contrasena +"')";

                Conexion insercionMascota = new Conexion(query);

                MessageBox.Show("Insercion realizada correctamente");
                numeroFilas = dataGridViewUsuarios.Rows.Count - 1;
            }
            else
            {
                String query = "UPDATE `usuarios` SET `nombre`='" + nombre + "',`apellidos`='" + apellidos + "',`dni`='" + dni + "',`contrasena`='" + contrasena + "'WHERE `id` = '" + id + "'";

                Conexion insercionMascota = new Conexion(query);

                MessageBox.Show("Insercion realizada correctamente");
                numeroFilas = dataGridViewUsuarios.Rows.Count - 1;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //Borrar usuario
            String idABorrar = dataGridViewUsuarios.CurrentRow.Cells["id"].Value.ToString();

            String query = "DELETE FROM `usuarios` WHERE `usuarios`.`id` = " + idABorrar;
            Conexion borradoMascota = new Conexion(query);
            rellenaList();
        }

        private void textBox1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                consulta = consulta.Substring(0, -1);
                string query = "SELECT * FROM USUARIOS WHERE nombre LIKE '%" + consulta + "%' OR apellidos  LIKE '%" + consulta + "%' OR dni  LIKE '%" + consulta + "%'";
                Conexion verUsuarios = new Conexion(query);

                DataTable datos = verUsuarios.resultadoConsulta(query);
                dataGridViewUsuarios.DataSource = datos;
            }
            else
            {
                consulta = ((TextBox)sender).Text;
                string query = "SELECT * FROM USUARIOS WHERE nombre LIKE '%" + consulta + "%' OR apellidos  LIKE '%" + consulta + "%' OR dni  LIKE '%" + consulta + "%'";
                Conexion verUsuarios = new Conexion(query);

                DataTable datos = verUsuarios.resultadoConsulta(query);
                dataGridViewUsuarios.DataSource = datos;
            }
            if (textBox1.Text == "")
            {
                consulta = "";
                string query = "SELECT * FROM USUARIOS";
                Conexion verUsuarios = new Conexion(query);

                DataTable datos = verUsuarios.resultadoConsulta(query);
                dataGridViewUsuarios.DataSource = datos;
            }
        }
    }
}
