﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.Types;
using MySql.Data.MySqlClient;

namespace veterinarioPepino
{
    public partial class Login : Form
    {
        
        private DataTable datos = new DataTable();
        //variable para localizar el raton
        public Point posicionRaton;
        public static String nombreUsuario;
        public Login()
        {
            InitializeComponent();
            
            String query = "Select * from usuarios";
            Conexion miloggeo = new Conexion(query);
            datos = miloggeo.resultadoConsulta(query);
            cargaImagen();
        }
        private void cargaImagen() {
            Random aleatorio = new Random();

           pictureBox2.Image = Image.FromFile(@"C:\Users\Dani\source\repos\veterinarioPepino\veterinarioPepino\huellas\" + aleatorio.Next(1,4) + ".png");
           //pictureBox2.Image = Image.FromFile(@"C:\Users\USUARIO\source\repos\veterinarioPepino\veterinarioPepino\huellas\" + aleatorio.Next(1, 4) + ".png");
        }
        private void button1_Click(object sender, EventArgs e)
        {
            
            String usuario = inputUsuario.Text;
            
            String contrasena = inputContrasena.Text;
            
            for (int i = 0; i<datos.Rows.Count; i++) {
                if (datos.Rows[i][3].ToString().Equals(usuario) && datos.Rows[i][4].ToString().Equals(contrasena))
                {
                    nombreUsuario = datos.Rows[i][1].ToString(); ;
                   
                    aviso.Text = "";
                    this.Hide();
                    //ventanaPrincipal miVentana = new ventanaPrincipal();
                    // miVentana.Show();
                    form inicio = new form();
                    inicio.Show();
                }
                

            }
            aviso.Text = "Usuario o contraseña incorrectos";
            inputUsuario.Text = "";
            inputContrasena.Text = "";
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            posicionRaton = new Point(-e.X,-e.Y);
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                Point posRaton = Control.MousePosition;
                posRaton.Offset(posicionRaton.X, posicionRaton.Y);
                Location = posRaton;
            }
        }
    }
}
