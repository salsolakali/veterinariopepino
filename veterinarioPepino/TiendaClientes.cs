﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace veterinarioPepino
{
    public partial class TiendaClientes : UserControl
    {
        int idDeLaPrimera;
        int idDeLaSegunda;
        int idDeLaTercera;
        int UltimaImagenCargada = 1;
        DataTable datos;
        DataColumn columna;
        DataRow fila;
        DataRow filaFinal;
        int Cuentacarrito = 0;
        Double precioTotal = 0;
        int unidadesTotales = 0;
        public static DataTable compras = new DataTable();
        private static TiendaClientes _instancia;
        
        public static TiendaClientes Instancia
        {
            get
            {
                if (_instancia == null)

                    _instancia = new TiendaClientes();
                return _instancia;


            }
        }
        public TiendaClientes()
        {
            InitializeComponent();
            cargaLasImagenesDelAlmacen();
            creaColumnas();
        }
        private void creaColumnas() {
            columna = new DataColumn();
            columna.DataType = System.Type.GetType("System.Int32");
            columna.ColumnName = "id";
            compras.Columns.Add(columna);

            // Segunda columna
            columna = new DataColumn();
            columna.DataType = Type.GetType("System.String");
            columna.ColumnName = "nombre";
            compras.Columns.Add(columna);
            // Tercera columna
            columna = new DataColumn();
            columna.DataType = Type.GetType("System.Int32");
            columna.ColumnName = "unidades";
            compras.Columns.Add(columna);
            // Cuarta columna
            columna = new DataColumn();
            columna.DataType = Type.GetType("System.Double");
            columna.ColumnName = "precio";
            compras.Columns.Add(columna);
            // Quinta columna
            columna = new DataColumn();
            columna.DataType = Type.GetType("System.String");
            columna.ColumnName = " ";
            compras.Columns.Add(columna);
            /*// Columna total
            columna = new DataColumn();
            columna.DataType = Type.GetType("System.Double");
            columna.ColumnName = "total";
            compras.Columns.Add(columna);*/

        }
        public void cargaLasImagenesDelAlmacen()
        {
            string query = "SELECT * FROM almacen";
            Conexion verProductos = new Conexion(query);

            datos = verProductos.resultadoConsulta(query);

            // int[] arrayProductosDisponibles = new int[datos.Rows.Count];

            //  for (int i = 0; i < datos.Rows.Count; i++) {
            // arrayProductosDisponibles[i] = (int)datos.Rows[i][0];
            // }
            cargaImagen(); 
        }
        public void cargaImagen() {
                for (int i = 0; i < 3; i++)
                {
                    if (UltimaImagenCargada > 10 || UltimaImagenCargada == 0)
                    {
                        UltimaImagenCargada = 1;
                    }
                    switch (i)
                    {
                         ////Ruta de Dani
                        case 0: imagen1.Image = Image.FromFile(@"C:\Users\dani\source\repos\veterinarioPepino\veterinarioPepino\imagenes\" + UltimaImagenCargada + ".png");
                        //Ruta de papi
                        //case 0: imagen1.Image = Image.FromFile(@"C:\Users\USUARIO\source\repos\veterinarioPepino\veterinarioPepino\imagenes\" + UltimaImagenCargada + ".png");
                        if ((datos.Rows[UltimaImagenCargada - 1][3]).ToString().Equals("0"))
                        {
                            button1.Enabled = false;
                        }
                        else
                        {
                            button1.Enabled = true;
                        }
                        idDeLaPrimera = UltimaImagenCargada-1;
                        UltimaImagenCargada++;
                        break;

                        //Ruta de dani
                        case 1: imagen2.Image = Image.FromFile(@"C:\Users\dani\source\repos\veterinarioPepino\veterinarioPepino\imagenes\" + UltimaImagenCargada + ".png");
                        //Papi
                        //case 1: imagen2.Image = Image.FromFile(@"C:\Users\USUARIO\source\repos\veterinarioPepino\veterinarioPepino\imagenes\" + UltimaImagenCargada + ".png");
                        if ((datos.Rows[UltimaImagenCargada - 1][3]).ToString().Equals("0"))
                        {
                            button2.Enabled = false;
                        }
                        else
                        {
                            button2.Enabled = true;
                        }
                        idDeLaSegunda = UltimaImagenCargada-1;
                        UltimaImagenCargada++;
                        break;
                        //Dani
                        case 2: imagen3.Image = Image.FromFile(@"C:\Users\dani\source\repos\veterinarioPepino\veterinarioPepino\imagenes\" + UltimaImagenCargada + ".png");

                        //Papi
                        //case 2: imagen3.Image = Image.FromFile(@"C:\Users\USUARIO\source\repos\veterinarioPepino\veterinarioPepino\imagenes\" + UltimaImagenCargada + ".png");
                        if ((datos.Rows[UltimaImagenCargada - 1][3]).ToString().Equals("0"))
                        {
                            button3.Enabled = false;
                        }
                        else
                        {
                            button3.Enabled = true;
                        }
                        idDeLaTercera = UltimaImagenCargada-1;
                        UltimaImagenCargada++;
                        break;

                       
                       
                        
                }

            }


            }    
            
        private void button5_Click(object sender, EventArgs e)
        {
            //Boton anterior
            if (UltimaImagenCargada < 4) {
                UltimaImagenCargada += 6;
            }
            if (UltimaImagenCargada >= 4) {
                UltimaImagenCargada -= 3;
                cargaImagen();
            }
            if (UltimaImagenCargada >= 6) {
                UltimaImagenCargada -= 6;
                cargaImagen();
            }
            
            //idDeLaPrimeraImagenQueVoyAMostrar -= 4;
            //cargaLasImagenesDelAlmacen(idDeLaPrimeraImagenQueVoyAMostrar);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            //Boton siguiente
            cargaImagen();
            //cargaLasImagenesDelAlmacen(idDeLaTerceraImagenQueVoyAMostrar);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            anadeCarrito(idDeLaPrimera, button1);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            anadeCarrito(idDeLaSegunda, button2);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            anadeCarrito(idDeLaTercera, button3); 
        }
        private void anadeCarrito(int id, Button boton) {
            Cuentacarrito++;
            labelCarrito.Text = Cuentacarrito.ToString();
            datos.Rows[id][3] = (int)datos.Rows[id][3] - 1;
            if (datos.Rows[id][3].ToString().Equals("0"))
            {
                boton.Enabled = false;
            }
            fila = compras.NewRow();
            fila["id"] = datos.Rows[id][0];
            fila["nombre"] = datos.Rows[id][1];
            fila["unidades"] = 1;
            fila["precio"] = (Double)datos.Rows[id][4];
            /*fila["iva"] = (Double)datos.Rows[id][4] * 0.23;
            fila["total"] = (Double)fila["precio"] + (Double)fila["iva"];*/
            precioTotal += (Double)fila["precio"];
            unidadesTotales++;
            
            //Como he añadido un producto al carrito, lo resto del stock

            String query = "UPDATE `almacen` SET `stock disponible`='" + (int)datos.Rows[id][3] + "' WHERE `idProducto` = '" + datos.Rows[id][0] + "'";
            Conexion insercionAlmacen = new Conexion(query);

            // MessageBox.Show("Añadido '"+ datos.Rows[id][1] + "' correctamente al carrito.");

            compras.Rows.Add(fila);
            /*string expresion;
            expresion = "nombre > '"+ datos.Rows[id][1] +"'";
            DataRow[] filaEncontrada;

            // Use the Select method to find all rows matching the filter.
            filaEncontrada = compras.Select(expresion);
            if (filaEncontrada != null)
            {
                foreach (DataRow datoFila in compras.Rows) // search whole table
                {
                    if (datoFila["nombre"] == datos.Rows[id][1]) // if id==2
                    { 
                        datoFila["unidades"] =  //change the name
                                                    //break; break or not depending on you
                    }
                }
            }
            else
            {*/

            //}
            // Print column 0 of each returned row.
            /*for (int i = 0; i < filaEncontrada.Length; i++)
            {
                Console.WriteLine(filaEncontrada[i][0]);
            }*/

        }
        private void button3_EnabledChanged(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            filaFinal = compras.NewRow();
            filaFinal[" "] = "Total articulos";
            filaFinal["unidades"] = unidadesTotales;
            filaFinal["precio"] = precioTotal;
            compras.Rows.Add(filaFinal);
            Factura miFactura = new Factura();
            miFactura.Show();


        }
        
        
    }
}
