﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace veterinarioPepino
{
    public partial class Clientes : UserControl
    {
        public int numeroFilas;
        String consulta = "";
        private static Clientes _instancia;
        public static Clientes Instancia

        {
            get
            {
                if (_instancia == null)
                
                    _instancia = new Clientes();

                return _instancia;
                

            }
        }
        public Clientes()
        {
           
                InitializeComponent();
                rellenaList();
                numeroFilas = dataGridViewClientes.Rows.Count -1;
            
        }
  
        public  void rellenaList()
        {
            string query = "SELECT * FROM CLIENTES";
            Conexion verClientes = new Conexion(query);

            DataTable datos = verClientes.resultadoConsulta(query);
            dataGridViewClientes.DataSource = datos;

        }

        private void button2_Click(object sender, EventArgs e)
        {
            //Boton de guardar
            String nombre = dataGridViewClientes.CurrentRow.Cells["nombre"].Value.ToString();
            String apellidos = dataGridViewClientes.CurrentRow.Cells["apellidos"].Value.ToString();
            String telefono = dataGridViewClientes.CurrentRow.Cells["telefono"].Value.ToString();
            String idCliente = dataGridViewClientes.CurrentRow.Cells["idCliente"].Value.ToString();
            if (dataGridViewClientes.CurrentRow.Index >= numeroFilas)
            {
                String query = "INSERT INTO `clientes`(`nombre`, `apellidos`, `telefono`) VALUES ('" + nombre + "','" + apellidos + "','" + telefono + "')";
                Conexion insercionCliente = new Conexion(query);
                
                MessageBox.Show("Insercion realizada correctamente");
                numeroFilas = dataGridViewClientes.Rows.Count - 1;
            }
            else
            {
                String query = "UPDATE `clientes` SET `nombre`='" + nombre + "',`apellidos`='" + apellidos + "',`telefono`='" + telefono + "' WHERE `idCliente` = '" + idCliente + "'";
                
                Conexion insercionCliente = new Conexion(query);

                MessageBox.Show("Insercion realizada correctamente");
                numeroFilas = dataGridViewClientes.Rows.Count - 1;
            }




        }

        private void textBox1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                consulta = consulta.Substring(0, -1);
                string query = "SELECT * FROM CLIENTES WHERE nombre LIKE '%" + consulta + "%' OR apellidos  LIKE '%" + consulta + "%' OR telefono  LIKE '%" + consulta + "%'";
                Conexion verClientes = new Conexion(query);

                DataTable datos = verClientes.resultadoConsulta(query);
                dataGridViewClientes.DataSource = datos;
            }
            else
            {
                consulta = ((TextBox)sender).Text;
                string query = "SELECT * FROM CLIENTES WHERE nombre LIKE '%" + consulta + "%' OR apellidos  LIKE '%" + consulta + "%' OR telefono  LIKE '%" + consulta + "%'";
                Conexion verClientes = new Conexion(query);

                DataTable datos = verClientes.resultadoConsulta(query);
                dataGridViewClientes.DataSource = datos;
            }
            if (textBox1.Text == "")
            {
                consulta = "";
                string query = "SELECT * FROM CLIENTES";
                Conexion verClientes = new Conexion(query);

                DataTable datos = verClientes.resultadoConsulta(query);
                dataGridViewClientes.DataSource = datos;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //Metodo de borrado
            String idABorrar = dataGridViewClientes.CurrentRow.Cells["idCliente"].Value.ToString();
            
            String query = "DELETE FROM `clientes` WHERE `clientes`.`idCliente` = " + idABorrar;
            Conexion borradoEntrada = new Conexion(query);
            rellenaList();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Boton de actualizar
            rellenaList();
        }
    }
}
