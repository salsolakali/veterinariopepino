﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace veterinarioPepino
{
    public partial class TiendaAdmin : UserControl
    {
        public int numeroFilas;
        String consulta = "";
        private static TiendaAdmin _instancia;
        public static TiendaAdmin Instancia
        {
            get
            {
                if (_instancia == null)

                    _instancia = new TiendaAdmin();
                return _instancia;


            }
        }
        public TiendaAdmin()
        {
            InitializeComponent();
            rellenaList();
            numeroFilas = dataGridViewAlmacen.Rows.Count - 1;
        }
        public void rellenaList()
        {
            string query = "SELECT * FROM almacen";
            Conexion verAlmacen = new Conexion(query);

            DataTable datos = verAlmacen.resultadoConsulta(query);
            dataGridViewAlmacen.DataSource = datos;

        }

        private void button5_Click(object sender, EventArgs e)
        {
            //Boton actualizar
            rellenaList();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            //Boton guardar
            String nombre = dataGridViewAlmacen.CurrentRow.Cells["nombre"].Value.ToString();
            String descripcion = dataGridViewAlmacen.CurrentRow.Cells["descripcion"].Value.ToString();
            String stock = dataGridViewAlmacen.CurrentRow.Cells["stock disponible"].Value.ToString();
            String precio = dataGridViewAlmacen.CurrentRow.Cells["precio"].Value.ToString();
            String idProducto = dataGridViewAlmacen.CurrentRow.Cells["idProducto"].Value.ToString();
            if (dataGridViewAlmacen.CurrentRow.Index >= numeroFilas)
            {
                String query = "INSERT INTO `almacen`(`nombre`, `descripcion`, `stock disponible`, `precio`) VALUES ('" + nombre + "','" + descripcion + "','" + stock + "','" + precio + "')";
                Conexion insercionAlmacen = new Conexion(query);

                MessageBox.Show("Insercion realizada correctamente");
                numeroFilas = dataGridViewAlmacen.Rows.Count - 1;
            }
            else
            {
                String query = "UPDATE `almacen` SET `nombre`='" + nombre + "',`descripcion`='" + descripcion + "',`stock disponible`='" + stock + "',`precio`='" + precio + "' WHERE `idProducto` = '" + idProducto + "'";
                System.Diagnostics.Debug.WriteLine(query);
                Conexion insercionAlmacen = new Conexion(query);

                MessageBox.Show("Insercion realizada correctamente");
                numeroFilas = dataGridViewAlmacen.Rows.Count - 1;
            }
        }
    

        private void button3_Click(object sender, EventArgs e)
        {
            //Boton borrar
            String idABorrar = dataGridViewAlmacen.CurrentRow.Cells["idProducto"].Value.ToString();

            String query = "DELETE FROM `almacen` WHERE `almacen`.`idProducto` = " + idABorrar;
            Conexion borradoEntrada = new Conexion(query);
            rellenaList();
        }

        private void textBox1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                consulta = consulta.Substring(0, -1);
                string query = "SELECT * FROM almacen WHERE nombre LIKE '%" + consulta + "%' OR descripcion  LIKE '%" + consulta + "%'";
                Conexion verClientes = new Conexion(query);

                DataTable datos = verClientes.resultadoConsulta(query);
                dataGridViewAlmacen.DataSource = datos;
            }
            else
            {
                consulta = ((TextBox)sender).Text;
                string query = "SELECT * FROM almacen WHERE nombre LIKE '%" + consulta + "%' OR descripcion  LIKE '%" + consulta + "%'";
                Conexion verClientes = new Conexion(query);

                DataTable datos = verClientes.resultadoConsulta(query);
                dataGridViewAlmacen.DataSource = datos;
            }
            if (textBox1.Text == "")
            {
                consulta = "";
                string query = "SELECT * FROM almacen";
                Conexion verClientes = new Conexion(query);

                DataTable datos = verClientes.resultadoConsulta(query);
                dataGridViewAlmacen.DataSource = datos;
            }
        }
    }
}
