﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.Types;
using MySql.Data.MySqlClient;
using System.Data;

namespace veterinarioPepino
{
    public class Conexion
    {
        private MySqlConnection conexion;
        private static MySqlCommand comando;
       // private String consulta;
        private MySqlDataReader resultado;
        private DataTable datos = new DataTable();

        public Conexion(String query) {

            

            resultadoConsulta(query);
        }
            public DataTable resultadoConsulta(String query){
                conexion = new MySqlConnection("Server = 127.0.0.1; Database=veterinario; Uid = root; Pwd=");
                conexion.Open();
                comando = new MySqlCommand(query, conexion);
                resultado = comando.ExecuteReader();
                datos.Load(resultado);
                conexion.Close();
                return datos;
            }
        }

        
    }

